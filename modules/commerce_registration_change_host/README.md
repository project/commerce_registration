CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration

INTRODUCTION
------------

Commerce Registration Change Host is a submodule of Commerce Registration that integrates with the Registration Change Host submodule of the Registration module. It allows site administrators to change the Product Variation that an existing registration is for.


CONFIGURATION
-------------

View the README file in the Registration Change Host submodule for information about setting the required permissions and choosing configuration options.