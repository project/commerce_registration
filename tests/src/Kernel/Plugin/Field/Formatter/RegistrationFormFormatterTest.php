<?php

namespace Drupal\Tests\commerce_registration\Kernel\Plugin\Field\Formatter;

use Drupal\Tests\commerce_registration\Traits\ProductCreationTrait;
use Drupal\Tests\commerce_registration\Traits\ProductVariationCreationTrait;

/**
 * Tests the registration_form formatter.
 *
 * @coversDefaultClass \Drupal\commerce_registration\Plugin\Field\FieldFormatter\RegistrationFormFormatter
 *
 * @group commerce_registration
 */
class RegistrationFormFormatterTest extends FormatterTestBase {

  use ProductCreationTrait;
  use ProductVariationCreationTrait;

  /**
   * @covers ::viewElements
   */
  public function testRegistrationFormFormatter() {
    $product = $this->createAndSaveProduct();
    $variation = $this->createAndSaveVariation($product);
    $build = $variation->get('event_registration')->view([
      'type' => 'registration_form',
      'label' => 'hidden',
    ]);
    $output = $this->renderPlain($build);
    $this->assertStringContainsString('<form class="registration-conference-register-form', $output);
  }

}
