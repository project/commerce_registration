<?php

namespace Drupal\commerce_registration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\registration\Plugin\Field\FieldFormatter\RegistrationLinkFormatter as BaseRegistrationLinkFormatter;

/**
 * Extends the plugin implementation of the 'registration_link' formatter.
 *
 * Adds a required route parameter for commerce products when needed.
 */
class RegistrationLinkFormatter extends BaseRegistrationLinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $cache_entities = [];
    if ($entity = $items->getEntity()) {
      /** @var \Drupal\registration\HostEntityInterface $host_entity */
      $host_entity = $this->entityTypeManager
        ->getHandler('registration', 'host_entity')
        ->createHostEntity($entity, $langcode);
      $settings = $host_entity->getSettings();
      $cache_entities[] = $settings;
      if (isset($items[0])) {
        if ($id = $items[0]->getValue()['registration_type']) {
          $registration_type = $this->entityTypeManager
            ->getStorage('registration_type')
            ->load($id);
          if ($registration_type) {
            $cache_entities[] = $registration_type;
            if ($host_entity->isEnabledForRegistration()) {
              $entity_type_id = $host_entity->getEntityTypeId();
              $url = Url::fromRoute("entity.$entity_type_id.registration.register", [
                $entity_type_id => $host_entity->id(),
              ]);
              // Add the extra route parameter for the parent product if needed.
              if ($entity_type_id == 'commerce_product_variation') {
                $parameters = $url->getRouteParameters();
                /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $entity */
                $parameters['commerce_product'] = $entity->getProduct()->id();
                $url->setRouteParameters($parameters);
              }
              $label = $this->getSetting('label') ?: $registration_type->label();
              $elements[] = [
                '#markup' => Link::fromTextAndUrl($label, $url)->toString(),
              ];
            }
          }
        }
      }
      $host_entity->addCacheableDependencies(
        $elements,
        $cache_entities
      );
    }
    return $elements;
  }

}
