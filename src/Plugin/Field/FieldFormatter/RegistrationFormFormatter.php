<?php

namespace Drupal\commerce_registration\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\registration\Plugin\Field\FieldFormatter\RegistrationFormFormatter as BaseRegistrationFormFormatter;

/**
 * Extends the plugin implementation of the 'registration_form' formatter.
 *
 * Allows this formatter to be used on a product page with multiple variations.
 * Adds the product variation host entity to the form state so the product
 * is not mistakenly used as the host entity instead.
 */
class RegistrationFormFormatter extends BaseRegistrationFormFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $cache_entities = [];
    if ($entity = $items->getEntity()) {
      /** @var \Drupal\registration\HostEntityInterface $host_entity */
      $host_entity = $this->entityTypeManager
        ->getHandler('registration', 'host_entity')
        ->createHostEntity($entity, $langcode);
      $settings = $host_entity->getSettings();
      $cache_entities[] = $settings;
      if (isset($items[0])) {
        if ($id = $items[0]->getValue()['registration_type']) {
          $registration_type = $this->entityTypeManager->getStorage('registration_type')->load($id);
          if ($registration_type) {
            $cache_entities[] = $registration_type;
            if ($host_entity->isEnabledForRegistration()) {
              $registration = $this->entityTypeManager->getStorage('registration')->create([
                'entity_type_id' => $host_entity->getEntityTypeId(),
                'entity_id' => $host_entity->id(),
                'type' => $registration_type->id(),
              ]);
              // Add the host entity to the form state.
              $elements[] = $this->entityFormBuilder->getForm($registration, 'register', [
                'host_entity' => $host_entity,
              ]);
            }
          }
        }
      }
      $host_entity->addCacheableDependencies(
        $elements,
        $cache_entities
      );
    }
    return $elements;
  }

}
